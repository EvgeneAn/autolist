from django.urls import path

from . import views

app_name = 'orders'

urlpatterns = [
    path('orders/', views.OrderListView.as_view()),
    path('orders/<int:pk>/', views.OrderUDView.as_view()),
    path('carlist/', views.CarListView.as_view()),
    path('carlist/<int:pk>/', views.CarListUDView.as_view())
]

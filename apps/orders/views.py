from datetime import date
from django.http import Http404
from django.db import transaction
from django.db.models import Sum
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import (OrderSerializer, OrderCRUDSerializer,
                          CarCRUDSerializer, FullCarListSerializer)
from .models import OrderAuto, CarList


class OrderListView(ListAPIView):
    '''
    Метод get поддерживает сортировку по количеству автомобилей в заказе и фильтрацию.
    Для сортировки нужно передаться параметр count=<asc/desc>, для фильтарции нужно передать
    парметр brand=<имя марки автомобиля>
    '''
    serializer_class = OrderSerializer

    def get_queryset(self):
        '''
        Выполняет сортировку и/или фильтрацию, если в запросе были переданы
        параметры count и/или brand.
        '''
        count = self.request.query_params.get('count')
        brand = self.request.query_params.get('brand')
        queryset = None
        sort = False
        filtering = False

        if count in ('asc', 'desc') and brand:
            # по возрастанию
            if count == 'asc':
                queryset = OrderAuto.objects.filter(cars__brand__brand_name=brand).annotate(sum=Sum('carlist__count')).order_by('sum')
                sort = True
            # по убыванию
            elif count == 'desc':
                queryset = OrderAuto.objects.filter(cars__brand__brand_name=brand).annotate(sum=Sum('carlist__count')).order_by('-sum')
                sort = True
        elif brand:
                queryset = OrderAuto.objects.filter(cars__brand__brand_name=brand).distinct()
                filtering = True
        elif count in ('asc', 'desc'):
            # по возрастанию
            if count == 'asc':
                queryset = OrderAuto.objects.all().annotate(sum=Sum('carlist__count')).order_by('sum')
                sort = True
            # по убыванию
            elif count == 'desc':
                queryset = OrderAuto.objects.all().annotate(sum=Sum('carlist__count')).order_by('-sum')
                sort = True

        if not sort and not filtering:
            queryset = OrderAuto.objects.order_by('pk')
        return queryset

    @transaction.atomic
    def post(self, request):
        response = {}
        cars_true = True

        if 'date_order' not in request.data:
            today = date.today()
            request.data['date_order'] = today.isoformat()

        # создает заказ
        serializer_order = OrderCRUDSerializer(data=request.data)
        if serializer_order.is_valid():
            order = serializer_order.save()

            # создает записи об автомобилях заказа,
            # добавляет к данным pk заказа
            for car in request.data['cars']:
                car['order_auto'] = order.pk
                serializer_car = CarCRUDSerializer(data=car)
                if serializer_car.is_valid():
                    serializer_car.save()
                else:
                    cars_true = False
                    name = 'index model_auto: ' + str(request.data['cars'].index(car))
                    response[name] = serializer_car.errors
        else:
            cars_true = False
            response['order'] = serializer_order.errors

        if cars_true:
            return Response(request.data, status=status.HTTP_201_CREATED)

        return Response(response)


class OrderUDView(APIView):
    def get_object(self, pk):
        try:
            return OrderAuto.objects.get(pk=pk)
        except OrderAuto.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        order = self.get_object(pk)
        serializer = OrderSerializer(order)
        return Response(serializer.data)

    def put(self, request, pk):
        order = self.get_object(pk)
        serializer = OrderCRUDSerializer(order, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(request.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        order = self.get_object(pk)
        order.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CarListView(ListAPIView):
    serializer_class = FullCarListSerializer
    queryset = CarList.objects.all()

    def post(self, request):
        serializer = CarCRUDSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CarListUDView(APIView):
    def get_object(self, pk):
        try:
            return CarList.objects.get(pk=pk)
        except CarList.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        car = self.get_object(pk)
        serializer = FullCarListSerializer(car)
        return Response(serializer.data)

    def put(self, request, pk):
        car = self.get_object(pk)
        serializer = CarCRUDSerializer(car, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(request.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        car = self.get_object(pk)
        car.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

from django.contrib import admin

from .models import OrderAuto, CarList


class PatternAdmin(admin.ModelAdmin):
    pass


admin.site.register(CarList, PatternAdmin)
admin.site.register(OrderAuto, PatternAdmin)

from rest_framework import serializers

from .models import OrderAuto, CarList
from carlist.models import ModelAuto, BrandAuto


'''Вспомогательные классы для работы с таблицами OrderAuto и CarList.'''


class ColorField(serializers.RelatedField):
    def to_representation(self, value):
        return value.description


class ProducerField(serializers.RelatedField):
    def to_representation(self, value):
        return value.name


class BrandNameField(serializers.RelatedField):
    def to_representation(self, value):
        return value.brand_name


class OrderModelSerializer(serializers.ModelSerializer):
    brand = BrandNameField(read_only=True)

    class Meta:
        model = ModelAuto
        fields = ('pk', 'model_name', 'brand')


'''Классы для работы с таблицами OrderAuto и CarList.'''


class CarListSerializer(serializers.ModelSerializer):
    model_auto = OrderModelSerializer(read_only=True)
    color_auto = ColorField(read_only=True)

    class Meta:
        model = CarList
        fields = ('model_auto', 'count', 'color_auto')


class FullCarListSerializer(serializers.ModelSerializer):
    model_auto = OrderModelSerializer(read_only=True)
    color_auto = ColorField(read_only=True)

    class Meta:
        model = CarList
        fields = ('pk', 'order_auto', 'model_auto', 'count', 'color_auto')


class CarCRUDSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarList
        fields = ('order_auto', 'model_auto', 'color_auto', 'count')


class OrderSerializer(serializers.ModelSerializer):
    cars = CarListSerializer(source='carlist_set',
                             read_only=True, many=True)

    producer = ProducerField(read_only=True)

    class Meta:
        model = OrderAuto
        fields = ('pk', 'producer', 'date_order', 'cars')
        depth = 1


class OrderCRUDSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAuto
        fields = ('producer', 'date_order')

from django.db import models
from django.core.validators import MinValueValidator

from carlist.models import ColorAuto, ModelAuto
from producers.models import Producer


class OrderAuto(models.Model):
    class Meta:
        verbose_name = 'Заказ автомобилей'
        verbose_name_plural = 'Заказы автомобилей'

    producer = models.ForeignKey(Producer, on_delete=models.CASCADE,
                                 verbose_name='Поставщик', blank=False)
    cars = models.ManyToManyField(ModelAuto, through='CarList',
                                  through_fields=('order_auto', 'model_auto'),
                                  verbose_name='Автомобили', blank=False)
    date_order = models.DateField(verbose_name='Дата заказа', blank=True)

    def __str__(self):
        return f'Заказ {self.pk}: {self.producer.name}'


class CarList(models.Model):
    class Meta:
        verbose_name = 'Состав заказа'
        verbose_name_plural = 'Составы заказов'

    order_auto = models.ForeignKey(OrderAuto, on_delete=models.CASCADE,
                                   verbose_name='Заказ', blank=False)
    model_auto = models.ForeignKey(ModelAuto, on_delete=models.PROTECT,
                                   verbose_name='Модель автомобиля',
                                   blank=False)
    color_auto = models.ForeignKey(ColorAuto, on_delete=models.PROTECT,
                                   verbose_name='Цвет автомобиля',
                                   blank=False)
    count = models.IntegerField(verbose_name='Количество', blank=False,
                                validators=[MinValueValidator(1)])

    def __str__(self):
        return f'Заказ {self.order_auto.pk}: ' \
               f'Марка {self.model_auto.brand.brand_name}: ' \
               f'Модель {self.model_auto.model_name}'

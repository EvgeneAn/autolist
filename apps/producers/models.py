from django.db import models


class Country(models.Model):
    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'

    fullname = models.CharField(verbose_name='Наименование полное',
                                blank=False, max_length=500)
    name = models.CharField(verbose_name='Наименование сокращенное',
                            blank=False, max_length=50)

    def __str__(self):
        return f'{self.pk}: {self.fullname}'


class Producer(models.Model):
    class Meta:
        verbose_name = 'Поставщик'
        verbose_name_plural = 'Поставщики'

    name = models.CharField(verbose_name='Наименование поставщика',
                            blank=False, max_length=100)
    address = models.CharField(verbose_name='Адрес поставщика',
                               blank=False, max_length=200)
    phone = models.CharField(verbose_name='Телефон поставщика',
                             blank=False, max_length=13)
    country = models.ForeignKey(Country, blank=False, max_length=200,
                                verbose_name='Страна поставщика',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.pk}: {self.name}'

from django.urls import path, include
from rest_framework import routers

from . import views

app_name = 'producers'

router = routers.SimpleRouter()
router.register(r'countries', views.CountryViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('producers/', views.ProducerListCreateView.as_view()),
    path('producers/<int:pk>/', views.ProducerUDView.as_view())
]

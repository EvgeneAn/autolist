from rest_framework import serializers

from .models import Producer, Country


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('pk', 'fullname', 'name')


class ProducerCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producer
        fields = ('name', 'address', 'phone', 'country')


class ProducerSerializer(serializers.ModelSerializer):
    country = CountrySerializer(read_only=True)

    class Meta:
        model = Producer
        fields = ('pk', 'name', 'address', 'phone', 'country')

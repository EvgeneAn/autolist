from rest_framework import viewsets

from .serializers import (CountrySerializer, ProducerSerializer,
                          ProducerCreateUpdateSerializer)
from .models import Country, Producer
from autolist.abstractviews import ListCreateMixin, DetailUDMixin


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class ProducerListCreateView(ListCreateMixin):
    serializer_class = ProducerSerializer
    create_update_serializer = ProducerCreateUpdateSerializer
    model = Producer


class ProducerUDView(DetailUDMixin):
    serializer_class = ProducerSerializer
    create_update_serializer = ProducerCreateUpdateSerializer
    model = Producer

from django.contrib import admin
from .models import ColorAuto, BrandAuto, ModelAuto


class PatternAdimn(admin.ModelAdmin):
    pass


admin.site.register(ColorAuto, PatternAdimn)
admin.site.register(BrandAuto, PatternAdimn)
admin.site.register(ModelAuto, PatternAdimn)

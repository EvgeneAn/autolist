from django.db.models import Count, F
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response

from autolist.abstractviews import ListCreateMixin, DetailUDMixin
from orders.models import CarList
from .models import ColorAuto, BrandAuto, ModelAuto
from .serializers import (ColorAutoSerializer, MinBrandSerializer, BrandSerializer,
                          ModelSerializer, MinModelSerializer, ColorCarsSerializer,
                          BrandCarsSerializer)


class ColorAutoViewSet(viewsets.ModelViewSet):
    queryset = ColorAuto.objects.all()
    serializer_class = ColorAutoSerializer


class BrandListCreateView(ListCreateMixin):
    serializer_class = BrandSerializer
    create_update_serializer = MinBrandSerializer
    model = BrandAuto


class BrandUDView(DetailUDMixin):
    serializer_class = BrandSerializer
    create_update_serializer = MinBrandSerializer
    model = BrandAuto


class ModelListCreateView(ListCreateMixin):
    serializer_class = ModelSerializer
    create_update_serializer = MinModelSerializer
    model = ModelAuto


class ModelUDView(DetailUDMixin):
    serializer_class = ModelSerializer
    create_update_serializer = MinModelSerializer
    model = ModelAuto


class ColorCarCountView(APIView):
    def get(self, request):
        queryset = CarList.objects.values(color=F('color_auto__description')).annotate(count=Count('count'))
        serializer = ColorCarsSerializer(queryset, many=True)
        return Response(serializer.data)


class BrandCarCountView(APIView):
    def get(self, request):
        queryset = CarList.objects.values(brand=F('model_auto__brand__brand_name')).annotate(count=Count('count'))
        serializer = BrandCarsSerializer(queryset, many=True)
        return Response(serializer.data)

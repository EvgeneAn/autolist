from django.urls import path, include
from rest_framework import routers

from . import views

app_name = 'carlist'

router = routers.SimpleRouter()
router.register(r'colors', views.ColorAutoViewSet)

urlpatterns = [
    path('colors/countcars/', views.ColorCarCountView.as_view()),
    path('', include(router.urls)),
    path('brands/countcars/', views.BrandCarCountView.as_view()),
    path('brands/', views.BrandListCreateView.as_view()),
    path('brands/<int:pk>/', views.BrandUDView.as_view()),
    path('models/', views.ModelListCreateView.as_view()),
    path('models/<int:pk>/', views.ModelUDView.as_view()),
]

from django.db import models
from django.core.validators import MinValueValidator

from producers.models import Country


class ColorAuto(models.Model):
    class Meta:
        verbose_name = 'Цвет автомобиля'
        verbose_name_plural = 'Цвета автомобилей'

    code = models.IntegerField(primary_key=True, verbose_name='Код цвета краски',
                               validators=[MinValueValidator(1)], blank=False)
    paint_color = models.CharField(verbose_name='Цвет краски', blank=False,
                                   max_length=50)
    description = models.CharField(verbose_name='Описание цвета для техпаспорта',
                                   blank=False, max_length=50)
    metallic = models.BooleanField(verbose_name='Металлик',
                                   blank=False)

    def __str__(self):
        return f'{self.code}: {self.paint_color}: {self.description}'


class BrandAuto(models.Model):
    class Meta:
        verbose_name = 'Марка автомобиля'
        verbose_name_plural = 'Марки автомобиля'

    brand_name = models.CharField(verbose_name='Марка автомобиля', blank=False,
                                  max_length=50)
    country = models.ForeignKey(Country, blank=False, on_delete=models.PROTECT,
                                verbose_name='Страна производитель')

    def __str__(self):
        return f'{self.brand_name}'


class ModelAuto(models.Model):
    class Meta:
        verbose_name = 'Модель автомобиля'
        verbose_name_plural = 'Модели автомобиля'

    brand = models.ForeignKey(BrandAuto, on_delete=models.PROTECT,
                              verbose_name='Марка автомобиля', blank=False)
    model_name = models.CharField(verbose_name='Модель', blank=False,
                                  max_length=20)

    def __str__(self):
        return f'{self.brand.brand_name} {self.model_name}'

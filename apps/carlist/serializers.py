from rest_framework import serializers

from .models import ColorAuto, BrandAuto, ModelAuto
from producers.serializers import CountrySerializer


class ColorAutoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ColorAuto
        fields = ('code', 'paint_color', 'description', 'metallic')


class MinBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = BrandAuto
        fields = ('brand_name', 'country')


class BrandSerializer(serializers.ModelSerializer):
    country = CountrySerializer(read_only=True)

    class Meta:
        model = BrandAuto
        fields = ('pk', 'brand_name', 'country')


class MinModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelAuto
        fields = ('model_name', 'brand')


class ModelSerializer(serializers.ModelSerializer):
    brand = MinBrandSerializer(read_only=True)

    class Meta:
        model = ModelAuto
        fields = ('pk', 'model_name', 'brand')


class ColorCarsSerializer(serializers.Serializer):
    color = serializers.CharField(read_only=True)
    count = serializers.IntegerField(read_only=True)


class BrandCarsSerializer(serializers.Serializer):
    brand = serializers.CharField(read_only=True)
    count = serializers.IntegerField(read_only=True)

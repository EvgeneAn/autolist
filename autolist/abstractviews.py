from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


'''
В данном модуле содержаться классы для реализации CRUD операций
и вывода списков.
'''


class ListCreateMixin(APIView):
    serializer_class = None
    create_update_serializer = None
    model = None

    def get(self, request):
        objects = self.__class__.model.objects.all()
        serializer = self.__class__.serializer_class(objects, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = self.__class__.create_update_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetailUDMixin(APIView):
    serializer_class = None
    create_update_serializer = None
    model = None

    def get_object(self, pk):
        try:
            return self.model.objects.get(pk=pk)
        except self.__class__.model.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        object = self.get_object(pk)
        serializer = self.__class__.serializer_class(object)
        return Response(serializer.data)

    def put(self, request, pk):
        object = self.get_object(pk)
        serializer = self.__class__.create_update_serializer(object, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        object = self.get_object(pk)
        object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

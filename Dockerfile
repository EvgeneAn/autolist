FROM python:3.9.5

RUN mkdir /app
WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
COPY ./requirements.txt /app
RUN pip install -r requirements.txt

COPY . /app

CMD ["python", "manage.py", "makemigrations"]
CMD ["python", "manage.py", "collectstatic"]
CMD ["python", "manage.py", "migrate"]
